public CadastroFornecedor{

	private ControleFornecedor controle;

	public CadastroFornecedor(){

		controle = new ControleFornecedor();

	}

	public void JButtonAdicionarPessoaFisica (...){
		//...		
		String nome = jTextFieldNome.getText();
		String telefone = jTextFieldTelefone.getText();
		String cpf = jTextFieldCpf.getText();

		PessoaFisica fornecedor = new PessoaFisica(nome, telefone, cpf);
		controle.adicionar(fornecedor);

	}

	public void JButtonAdicionarPessoaJuridica (...){
		//...
		String nome = jTextFieldNome.getText();
		String telefone = jTextFieldTelefone.getText();
		String cnpj = jTextFieldCnpj.getText();

		PessoaJuridica fornecedor = new PessoaJuridica(nome, telefone, cnpj);
		controle.adicionar(fornecedor);


	}

}
