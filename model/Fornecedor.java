package model;

import java.util.ArrayList;

public class Fornecedor {

	private String nome;
	private String telefone;
	private Endereco localizacao;
	private ArrayList<Produto> produtos;

	public Fornecedor (String nome, String telefone){
		this.nome = nome;
		this.telefone = telefone;
	}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Endereco getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(Endereco localizacao) {
        this.localizacao = localizacao;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }     

}

