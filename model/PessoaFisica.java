package model;

public class PessoaFisica extends Fornecedor {

	private String cpf;


	public PessoaFisica(String nome, String telefone){
		super(nome,telefone);
	}

	public PessoaFisica(String nome, String telefone, String cpf){
		super(nome,telefone);
		this.cpf = cpf;
	}

	public void setCpf(String cpf){
		this.cpf = cpf;	
	}

}
