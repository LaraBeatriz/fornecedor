package controller;


import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;
import java.util.ArrayList;

public class controleFornecedor {
	private ArrayList<Fornecedor> listaFornecedores;

 	public controleFornecedor(){
		listaFornecedores = new ArrayList<Fornecedor>();
	}

	public String adicionar(PessoaFisica fornecedor){
		listaFornecedores.add(fornecedor);
		return "Fornecedor Pessoa Física adicionado com sucesso";	
	}

	public String adicionar(PessoaJuridica fornecedor){
		listaFornecedores.add(fornecedor);
		return "Fornecedor Pessoa Jurídica adicionado com sucesso";
	}
        
        public String remover (PessoaJuridica fornecedor){
            listaFornecedores.remove(fornecedor);
            return "Fornecedor Pessoa Jurídica removido com sucesso";
        }
        
        public String remover (PessoaFisica fornecedor){
            listaFornecedores.remove(fornecedor);
            return "Fornecedor Pessoa Jurídica removido com sucesso";
        }
        
        public Fornecedor pesquisarFornecedor (String umNome) {
        for (Fornecedor umFornecedor : listaFornecedores) {
            if (umFornecedor.getNome().equalsIgnoreCase(umNome)) {
                return umFornecedor;
            }
        }
        return null;
    }
}
