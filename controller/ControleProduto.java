/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import java.util.ArrayList;
import model.Produto;

/**
 *
 * @author Lara10
 */
public class ControleProduto {
    private ArrayList<Produto> listaProdutos;
    
    public ControleProduto (){
           listaProdutos = new ArrayList<Produto>();
    }
    
    public String adicionarProduto (Produto umProduto){
        listaProdutos.add(umProduto);
        return "Produto adicionado com sucesso!";
    }
    
    public String removerProduto (Produto umProduto){
        listaProdutos.remove(umProduto);
        return "Produto removido com sucesso!";
    }
    
    public Produto pesquisarProduto (String nomeProduto){
        for (Produto umProduto : listaProdutos) {
            if (umProduto.getNome().equalsIgnoreCase(nomeProduto)) {
                return umProduto;
            } 
        }
        return null;
    }
    
}
